- include_vars: prometheus-vars.yml
  tags: always

- name: "{{ prometheus_config_dir }}/dynamic"
  file:
    path: "{{ prometheus_config_dir }}/dynamic"
    state: directory
    recurse: yes

- name: "{{ prometheus_config_dir }}/prometheus.yml"
  copy:
    dest: "{{ prometheus_config_dir }}/prometheus.yml"
    content: |
      scrape_configs:
        - job_name: metrics           # Should be relabeled - This is just for the /targets UI
          file_sd_configs:
          - files:
            - /etc/prometheus/dynamic/metrics_targets.yml
        - job_name: gitlab_federate   # Ditto
          metrics_path: /federate
          params:
            'match[]':
              - '{__name__=~".+"}'
          # We want to override `instance` (hence no `honor_labels`),
          # but conversely we want to keep `job` as the federated
          # in-container Prometheus intended it:
          metric_relabel_configs:
            - source_labels:
                - exported_job
              target_label: job
            - target_label: exported_job
              replacement: ''
            - target_label: service
              replacement: gitlab
          file_sd_configs:
          - files:
            - /etc/prometheus/dynamic/gitlab_federate_targets.yml
  register: _prometheus_config_file

- name: "`prometheus` container"
  docker_container:
    name: prometheus
    image: "{{ prometheus_image }}"
    restart: >-
      {{ (_prometheus_config_file is defined) and (_prometheus_config_file is changed) }}
    restart_policy: unless-stopped
    log_driver: json-file
    log_options:
      max-size: 10m
      max-file: "3"
    network_mode: "{{ docker_network_name }}"
    volumes:
      - "{{ prometheus_config_dir }}:/etc/prometheus"

- name: "Watch Docker containers and set up Prometheus targets for them"
  epfl_si.docker.docker_observer:
    name: prometheus-discovery
    volumes:
      - "{{ prometheus_config_dir }}/dynamic:/prometheus_dynamic"
    python_script: |
      @on_docker_containers_changed
      async def prometheus_static_config_of_container (containers):
          metrics_targets = []
          gitlab_federate_targets = []

          for container in containers:
              details = await container.show()
              name = details.get("Name").strip("/")
              if not name:
                  pass

              extra_labels = dict(environment = "staging" if "staging" in name else "prod")

              if "GitlabStatsExporter" in name:
                  metrics_targets.append(static_config(container, name, port=3000,
                                                       **extra_labels))
              elif name.startswith("gitlab"):
                  metrics_targets.append(static_config(container, name, port=9090, **extra_labels))
                  gitlab_federate_targets.append(static_config(container, name, port=9090, **extra_labels))
              elif "postgres-exporter" in name:
                  metrics_targets.append(static_config(container, name, port=9187, **extra_labels))
              elif "traefik" in name:
                  metrics_targets.append(static_config(container, name, port=8080, **extra_labels))

          write_yaml_file("/prometheus_dynamic/metrics_targets.yml", metrics_targets)
          write_yaml_file("/prometheus_dynamic/gitlab_federate_targets.yml", gitlab_federate_targets)

      def static_config(container, name, port, **extra_labels):
          return dict(
                targets=["%s:%d" % (name, port)],
                labels=dict(
                    job=name,
                    instance="docker-%s" % container["Id"][:12],
                    **extra_labels))

  tags:
    - monitoring.prometheus.discovery
