# gitlab-docker

This is an EPFL-specific fixture of [https://gitlab.com/](gitlab), running
inside Docker and deployed with Ansible.

